from flask import Flask, jsonify,request,session
from flask_pymongo import PyMongo
from flask_cors import CORS
from werkzeug.wrappers import response
from chat import get_response
import config,json,uuid,secrets
config.setup_Api()
import infermedica_api
import train

evidence = [] #golbal evidence declaration

evidence = []

app = Flask(__name__)
CORS(app) # very important!
app.config["MONGO_URI"] = "mongodb://localhost:27017/test_DB"
app.secret_key =secrets.token_bytes(8)

mongodb_client = PyMongo(app)
db = mongodb_client.db


@app.get("/")
def server_status():
    return "server is up and running, happy coding :)"

@app.post("/initial") ### initial endpoint for a user to use symptom checker
def initial_setup():

    ''' Example payload
    {
    "age":"25",
    "age_unit":"year",
    "sex":"male",
    
    }'''
    session.clear()
    payload = request.json
    session['interviewID']=uuid.uuid4().hex
    session["age"] = int(payload["age"])
    session["age_unit"] = payload["age_unit"]
    session["sex"] =payload["sex"]
    #session["enable_third_person_questions"]="true"
    return session['interviewID']

@app.post("/diagnosis_start")  ### start diagonosis symptom checker
def diagnosis_start():

    ''' Example payload
    {
    "message":"i am feeling headache and vomiting",
    "extras":""
    }'''
    payload = request.json
    api: infermedica_api.APIv3Connector = infermedica_api.get_api()
    # interview_id=payload["interviewID"]
    extras=""
    message=payload["message"]
    ParseData=api.parse(message,session["age"])
    ParseDump=json.dumps(ParseData)
    loadParseDump = json.loads(ParseDump)
    mentions = loadParseDump["mentions"]
    #evidence = []
    for key, value in enumerate(mentions):
     data_set = {}
     #data_set = {"id": value['id'],  "common_name": value['common_name'],  "choice_id": value['choice_id']}
     data_set = {"id": value['id'],"choice_id": value['choice_id'],"source":"initial"}
     evidence.insert(key, data_set)
     session["evidence"] = evidence
    # call suggest for more evidence
    extras=""
    suggest_method=""
    max_results= 4
    diagnosis_start = api.suggest(session["evidence"],session["sex"],session["age"],session["age_unit"],extras,suggest_method,max_results,session['interviewID'])
    print(evidence)
    return  json.dumps(diagnosis_start)

@app.post("/pre_interview")   #endpoint for other source endpoints of infermedica
def pre_interview():
    payload = request.json
    data_set = {}          
    data_set = {"id": payload['id'],"choice_id": payload['choice_id'], "source": payload['source']}
    index = len(evidence) 
    print(index) #printing evidence len
    evidence.insert(index, data_set) #evidence list update
    session["evidence"] = evidence   
    # init api infermedica
    api: infermedica_api.APIv3Connector = infermedica_api.get_api()
    # call diagnosis with sources of other endpoints for first interview question
    extras=""
    interview_start = api.diagnosis(session["evidence"],session["sex"],session["age"],session["age_unit"],extras,session['interviewID'])
    return json.dumps(interview_start)
   

@app.post("/interview") #this is the endpoint for interview
def interview():
    payload = request.json
    #evidence = []
    data_set = {}
    data_set = {"id": payload['id'],"choice_id": payload['choice_id']} 
    index = len(evidence) 
    print(index) #printing evidence len
    evidence.insert(index, data_set) #evidence list update
    session["evidence"] = evidence   
    # init api infermedica
    api: infermedica_api.APIv3Connector = infermedica_api.get_api()
    # call diagnosis after staring interview
    extras=""
    interview_start = api.diagnosis(session["evidence"],session["sex"],session["age"],session["age_unit"],extras,session['interviewID'])
    return json.dumps(interview_start)

@app.post("/triage") ## triage endpoint , it should hit after interview returns should stop true for more consultation
def triage():
    api: infermedica_api.APIv3Connector = infermedica_api.get_api()
    # call triage method
    extras=""
    triage_response = api.triage(session["evidence"],session["sex"],session["age"],session["age_unit"],extras,session['interviewID'])
    return json.dumps(triage_response)

@app.post("/recommend_specialist") ## recommend_specialist  endpoint , it should hit after interview returns should stop true for specialist consultation
def recommend_specialist ():
    api: infermedica_api.APIv3Connector = infermedica_api.get_api()
    # call recommend_specialist method
    extras=""
    recommend_specialist_response = api.recommend_specialist(session["evidence"],session["sex"],session["age"],session["age_unit"],extras,session['interviewID'])
    return json.dumps(recommend_specialist_response)


@app.post("/healthbuddy")
def bot():
    text = request.json
    #TODO: check the text validity
    response = get_response(text["message"])
    reply = {"response": response}
    return reply

@app.post("/train_model")       #Endpoint to train the model
def train_model(): 
    train.modeltrain()
    return ("training complete. file saved to data.pth")



if __name__=="__main__":
   app.run(host='0.0.0.0',port=4567)     

