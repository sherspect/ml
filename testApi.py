import config
import json
config.setup_Api()
import infermedica_api

if __name__ == "__main__":
    api: infermedica_api.APIv3Connector = infermedica_api.get_api()

    age = 25

    # print("Parse simple text:")
    # response = api.parse("i am feeling hedche", age=age, include_tokens=True)
    # print(json.dumps(response))

    # print("Parse simple text and include tokens information:")
    # response = api.parse(
    #     "i am unable to talk and feeling numb and i am short of hear", age=age, include_tokens=True
    # )
    # print(response, end="\n\n")
    
    # print("Look for symptoms and risk factors containing the phrase from mentions:")
    # print(
    #     api.search(
    #         "headache",
    #         age=age,
    #         types=[
    #             infermedica_api.SearchConceptType.SYMPTOM,
    #             infermedica_api.SearchConceptType.RISK_FACTOR,
    #         ],
    #     ),
    #     end="\n\n",
    # )
    # request = config.get_example_request_data()

    # # call parse
    # request = api.search("fever",age=age)
    print("Look for symptoms and risk factors containing the phrase trauma:")
    request=api.search(
            "trauma",
            age=age,
            types=[
                infermedica_api.SearchConceptType.SYMPTOM,
                infermedica_api.SearchConceptType.RISK_FACTOR,
            ],
        )

    print(json.dumps(request))