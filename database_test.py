import pymongo
from flask import Flask
from flask_cors import CORS
from bson import json_util, ObjectId
import json
app = Flask(__name__)
CORS(app) # very important!


client = pymongo.MongoClient("mongodb://hospitalcare:rkdx.HD7k6%24p9fH@165.232.183.130:27017/?authSource=admin&readPreference=primary&appname=MongoDB%20Compass&directConnection=true&ssl=false")

# Database Name
db = client["intents"]

# Collection Name
col = db["patterns"]

@app.route("/")   #for checking server status
def home():
    status = "API2 is up and running"
    return (status)
  

@app.route("/fetch") #checking connection
def find():
    x = col.find_one()
    print (x)
    return json.loads(json_util.dumps(x))

    


if __name__ == "__main__":
    app.run(host='0.0.0.0',debug=True)
